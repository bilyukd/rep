<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $login_hash;

    private $_user;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */

    public function login()
    {
        $this->login_hash = Yii::$app->getSecurity()->generateRandomString();
        $user = $this->getUser();
        $user->setAttribute('login_hash', $this->login_hash);
        if ($this->validate()) {
            $user->setAttribute('email', $this->email);
        }
        $user->save();

        return $this->login_hash;
    }

    public function loginHash()
    {
        $user = User::findByHash($this->login_hash);
        if (is_null($user)) {
            return false;
        } else {
            $user->updateAttributes(['login_hash' => '']);
            return Yii::$app->getUser()->login($user);
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->email);
        }

        return $this->_user;
    }
}
