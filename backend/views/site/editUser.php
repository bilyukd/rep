<?
use yii\helpers\Url;

$this->title = 'Edit user';
$this->params['breadcrumbs'][] = $this->title;

?><table class="table">
    <thead>
        <tr> <th>id</th> <th>email</th> <th>edit link</th> </tr>
    </thead>
    <tbody>
        <?
        foreach($data as $row):?>
            <tr> <th scope="row"><?=$row['id']?></th> <td><?=$row['email']?></td> <td><a href="<?=Url::to(['site/editoneuser', 'id' => $row['id']])?>">Edit link</a> </td> </tr>
        <?endforeach;
        ?>
    </tbody>
</table>

