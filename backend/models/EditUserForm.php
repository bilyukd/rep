<?php
/**
 * Created by PhpStorm.
 * User: PC4
 * Date: 18.08.2017
 * Time: 16:39
 */

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class EditUserForm extends Model {
    public $email;
    public $id;

    public function rules()
    {
        return [
            [['email', 'id'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User'],
            ['id', 'integer'],
        ];
    }

    public function getAllUsers()
    {
        $user = new User;
        return $user->find()->asArray()->all();
    }

    public function editoneuser()
    {
        if ($this->validate()) {
            $user = User::findIdentity($this->id);
            $user->email = $this->email;
            return $user->save(false);
        }
    }
}