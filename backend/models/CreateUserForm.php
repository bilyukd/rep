<?php
/**
 * Created by PhpStorm.
 * User: PC4
 * Date: 18.08.2017
 * Time: 15:51
 */

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class CreateUserForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            // username and password are both required
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User'],
        ];
    }

    public function addUser() {
        $user = new User;
        if ($this->validate())
        {
            $user->setAttribute('email', $this->email);
            return $user->save();
        }
        return false;
    }
}